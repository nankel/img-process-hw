import cv2
import numpy as np
from moviepy.editor import VideoFileClip

blur_ksize = 5
canny_lthreshold = 50
canny_hthreshold = 150

# Hough transform parameters
rho = 1  # 设置极径分辨率
theta = np.pi / 180  # 设置极角分辨率
threshold = 15  # 设置检测一条直线所需最少的交点
min_line_length = 40  # 设置线段最小长度
max_line_gap = 20  # 设置线段最近俩点的距离

# 提取ROI区域
def roi_mask(img, vertices): 
    mask = np.zeros_like(img)
    if len(img.shape) > 2:
        channel_count = img.shape[2]
        mask_color = (255,) * channel_count
    else:
        mask_color = 255

    cv2.fillPoly(mask, vertices, mask_color)
    masked_img = cv2.bitwise_and(img, mask)
    return masked_img

# 霍夫变换，返回画好车道线的图片
def hough_lines(img, rho, theta, threshold, min_line_len, max_line_gap):  
    lines = cv2.HoughLinesP(img, rho, theta, threshold, np.array([]), minLineLength=min_line_len,
                            maxLineGap=max_line_gap)
    line_img = np.zeros((img.shape[0], img.shape[1], 3), dtype=np.uint8)
    draw_lanes(line_img, lines)
    return line_img

# 首先将霍夫变换获得的直线进行分类，然后分别再拟合出左右两条直线
def draw_lanes(img, lines, color=None, thickness=8):  
    if color is None:
        color = [255, 0, 0]
    left_lines, right_lines = [], []
    for line in lines:
        for x1, y1, x2, y2 in line:
            k = (y2 - y1) / (x2 - x1)
            if k < 0:
                left_lines.append(line)
            else:
                right_lines.append(line)

    if (len(left_lines) <= 0 or len(right_lines) <= 0):
        return img

    clean_lines(left_lines, 0.1)
    clean_lines(right_lines, 0.1)
    left_points = [(x1, y1) for line in left_lines for x1, y1, x2, y2 in line]
    left_points = left_points + [(x2, y2) for line in left_lines for x1, y1, x2, y2 in line]
    right_points = [(x1, y1) for line in right_lines for x1, y1, x2, y2 in line]
    right_points = right_points + [(x2, y2) for line in right_lines for x1, y1, x2, y2 in line]

    left_vtx = calc_lane_vertices(left_points, 325, img.shape[0])
    right_vtx = calc_lane_vertices(right_points, 325, img.shape[0])

    cv2.line(img, left_vtx[0], left_vtx[1], color, thickness)
    cv2.line(img, right_vtx[0], right_vtx[1], color, thickness)

# 去掉偏离均值较远的直线，这里的值是指斜率
def clean_lines(lines, threshold):  
    slope = [(y2 - y1) / (x2 - x1) for line in lines for x1, y1, x2, y2 in line]
    while len(lines) > 0:
        mean = np.mean(slope)
        diff = [abs(s - mean) for s in slope]
        idx = np.argmax(diff)
        if diff[idx] > threshold:
            slope.pop(idx)
            lines.pop(idx)
        else:
            break

# 将左右两侧的直线拟合，返回左右车道线的顶点
def calc_lane_vertices(point_list, ymin, ymax):  
    x = [p[0] for p in point_list]
    y = [p[1] for p in point_list]
    fit = np.polyfit(y, x, 1)
    fit_fn = np.poly1d(fit)

    xmin = int(fit_fn(ymin))
    xmax = int(fit_fn(ymax))

    return [(xmin, ymin), (xmax, ymax)]

# 总的处理函数，输入一张图片，返回一张标记车道线的图片
def process_an_image(img):  
    roi_vtx = np.array([[(0, img.shape[0]), (460, 325), (520, 325), (img.shape[1], img.shape[0])]])
    gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)  # 将图片转换为RGB表示的数组，并进行灰度处理
    blur_gray = cv2.GaussianBlur(gray, (blur_ksize, blur_ksize), 0, 0)  # 高斯滤波
    edges = cv2.Canny(blur_gray, canny_lthreshold, canny_hthreshold)  # canny算子进行边缘检测
    roi_edges = roi_mask(edges, roi_vtx)  # 提取感兴趣区域
    line_img = hough_lines(roi_edges, rho, theta, threshold, min_line_length, max_line_gap)  # 霍夫变换，提取直线段
    res_img = cv2.addWeighted(img, 0.8, line_img, 1, 0)
    return res_img


mp4_path = 'video_1.mp4'
out_path = 'video_1_out.mp4'
clip = VideoFileClip(mp4_path)
out_clip = clip.fl_image(process_an_image)
out_clip.write_videofile(out_path, audio=False)
