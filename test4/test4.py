import cv2
import numpy as np


img = cv2.imread("3.jpg",0)
cv2.imshow('img',img)
blur=cv2.GaussianBlur(img,(3,3),1,1)#核尺寸通过对图像的调节自行定义
# LoG检测器
  #调用Laplacian算法的OpenCV库函数进行图像轮廓提取
result = cv2.Laplacian(blur,cv2.CV_16S,ksize=1)
LOG = cv2.convertScaleAbs(result) #得到LOG算法处理结果
cv2.imshow('LOG',LOG)

# Scharr算子
Scharr=cv2.Scharr(blur, cv2.CV_16S, 1, 0)
Scharr = cv2.convertScaleAbs(Scharr)   # 转回uint8
cv2.imshow('Scharr',Scharr)

# Canny边缘检测器
Canny = cv2.Canny(blur, 50, 150)
cv2.imshow('Canny',Canny)
cv2.waitKey(0)
cv2.destroyAllWindows()
