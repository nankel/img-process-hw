import cv2
import numpy as np
import matplotlib.pyplot as plt  
#读取图片
img = cv2.imread('1.jpg',0)
cv2.imshow('img',img)
cv2.waitKey(0)
# 绘制灰度直方图
hist = cv2.calcHist([img], [0], None, [256], [0, 255]) 
plt.plot(hist)
plt.show()
# 直方图均衡
equ = cv2.equalizeHist(img)
cv2.imshow('equ',equ)
cv2.waitKey(0)
# 空域滤波(平滑滤波)
average_blur = cv2.blur(img, (3, 5))  # 模板大小为3*5, 模板的大小是可以设定的
box = cv2.boxFilter(img, -1, (3, 5))

gauss_blur = cv2.GaussianBlur(img, (5, 5), 0)  # （5,5）表示的是卷积模板的大小，0表示的是

median_blur = cv2.medianBlur(img, 5)

cv2.imshow('average_blur',average_blur)
cv2.imshow('box',box)
cv2.imshow('gauss_blur',gauss_blur)
cv2.imshow('median_blur',median_blur)
cv2.waitKey(0)

# 空域滤波(锐化滤波)
  #Roberts算子
ret,thresh1=cv2.threshold(gauss_blur,127,255,cv2.THRESH_BINARY)  #二进制阈值化
    #调用Roberts算法的OpenCV库函数进行图像轮廓提取
kernelx = np.array([[-1,0],[0,1]], dtype=int)
kernely = np.array([[0,-1],[1,0]], dtype=int)
x = cv2.filter2D(thresh1, cv2.CV_16S, kernelx)
y = cv2.filter2D(thresh1, cv2.CV_16S, kernely)
    #转uint8
absX = cv2.convertScaleAbs(x)
absY = cv2.convertScaleAbs(y)
Roberts = cv2.addWeighted(absX,0.5,absY,0.5,0)

cv2.imshow("Roberts",Roberts)

  # Prewitt算子
kernelx = np.array([[1,1,1],[0,0,0],[-1,-1,-1]],dtype=int)
kernely = np.array([[-1,0,1],[-1,0,1],[-1,0,1]],dtype=int)
x = cv2.filter2D(thresh1, -1, kernelx)
y = cv2.filter2D(thresh1, -1, kernely)
    #转uint8
absX = cv2.convertScaleAbs(x)
absY = cv2.convertScaleAbs(y)
Prewitt = cv2.addWeighted(absX,0.5,absY,0.5,0)
cv2.imshow("Prewitt",Prewitt)

  # Sobel算子
x = cv2.Sobel(thresh1, cv2.CV_16S, 1, 0) #对x求一阶导
y = cv2.Sobel(thresh1, cv2.CV_16S, 0, 1) #对y求一阶导
absX = cv2.convertScaleAbs(x)  #对x取绝对值，并将图像转换为8位图
absY = cv2.convertScaleAbs(y)   #对y取绝对值，并将图像转换为8位图
Sobel = cv2.addWeighted(absX, 0.5, absY, 0.5, 0)
cv2.imshow("Sobel",Sobel)
cv2.waitKey(0)
