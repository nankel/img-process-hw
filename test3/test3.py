import cv2
import numpy as np

img = cv2.imread("2.jpg",0)
cv2.imshow('img',img)
# 腐蚀
kernel = np.ones((3,3),np.uint8)
erosion = cv2.erode(img,kernel)
cv2.imshow('eroded image',erosion)

# 膨胀
dilation = cv2.dilate(img,kernel,iterations = 1)
cv2.imshow('dilation',dilation)

# 开运算：先腐蚀再膨胀
opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
cv2.imshow('opening',opening)

# 闭运算：先膨胀再腐蚀
closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)
cv2.imshow('closing',closing)
cv2.waitKey(0)
cv2.destroyAllWindows()

