import cv2
import numpy as np

class LPR_t:
    """ 车牌识别类
    参数说明：原始输入图像：imgRaw，车牌切割图像：imgLP，车牌灰度图：imgLP_gray，车牌二值图：imgLP_binary，车牌反色二值图：imgLP_reverse_binary,
           水平投影图像：imgLP_horizonProj，垂直投影图像：imgLP_vertialProj，矩形图框出的车牌字符图像：img_division_rectangle，分割字符图像：imgLP_slice_part
    """
    def __init__(self):
        pass
    # 对图片进行处理
    def img_process(self, imgPosition):
        resultIndex = []
        self.carNumber = ""
        self.imgRaw = cv2.imread(imgPosition)
        self.__find_plate_position__()
        self.__imgLP_process__()
        self.imgLP_horizonProj, self.imgLP_vertialProj, self.img_division_rectangle, self.imgLP_slice_part = self.__img_division__(self.imgLP_reverse_binary, 1)

        resultIndex.append(self.__chara_compare__("5-carNumber/chinese", self.imgLP_slice_part[0]))
        resultIndex.append(self.__chara_compare__("5-carNumber/english", self.imgLP_slice_part[1]))
        resultIndex.append(self.__chara_compare__("5-carNumber/english", self.imgLP_slice_part[2]))
        resultIndex.append(self.__chara_compare__("5-carNumber/english", self.imgLP_slice_part[3]))
        resultIndex.append(self.__chara_compare__("5-carNumber/number", self.imgLP_slice_part[4]))
        resultIndex.append(self.__chara_compare__("5-carNumber/number", self.imgLP_slice_part[5]))
        resultIndex.append(self.__chara_compare__("5-carNumber/number", self.imgLP_slice_part[6]))
        print(resultIndex)
        self.carNumber += self.__read_txt__("5-carNumber/chinese.txt", resultIndex[0]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/english.txt", resultIndex[1]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/english.txt", resultIndex[2]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/english.txt", resultIndex[3]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/number.txt", resultIndex[4]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/number.txt", resultIndex[5]).replace('\n', '').replace('\r', '') 
        self.carNumber += self.__read_txt__("5-carNumber/number.txt", resultIndex[6]).replace('\n', '').replace('\r', '') 

    # 找到图片中车牌的位置，用矩形在原图上框出来，然后切割出车牌图片
    def __find_plate_position__(self):
        (h,w) = self.imgRaw.shape[:2]  # 返回高和宽
        flag = 0
        maxj = 0
        for i in range(h):  # 行
            for j in range(w):  # 列
                Rij = self.imgRaw[i,j,2]
                Gij = self.imgRaw[i,j,1]
                Bij = self.imgRaw[i,j,0]
                if ((Rij/Bij<0.35 and Gij/Bij<0.9 and Bij>90)):
                # if ((Gij/Bij<0.35 and Rij/Bij<0.9 and Bij < 90)):
                    if flag == 0:
                        position_0 = (j,i)
                        flag = 1
                    if maxj < j:
                        maxj = j
                    position_1 = (maxj,i)
                    # print(i,j)
        cv2.rectangle(self.imgRaw, position_0, position_1, (0, 0, 255), 2)
        # 对车牌区域进行分割
        self.imgLP = self.imgRaw[position_0[1]:position_1[1], position_0[0]:position_1[0]]

    # 对车牌图像进行处理，包括灰度化，二值化，反色
    def __imgLP_process__(self):
        self.imgLP_gray = cv2.cvtColor(self.imgLP, cv2.COLOR_BGR2GRAY)
        ret,self.imgLP_binary = cv2.threshold(self.imgLP_gray, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)  # 0是黑色

        # 反色
        self.imgLP_reverse_binary = self.imgLP_binary.copy()
        for i in range(0, self.imgLP_binary.shape[0]):
            for j in range(0, self.imgLP_binary.shape[1]):
                self.imgLP_reverse_binary[i,j] = 255 - self.imgLP_binary[i,j]

    # 图像分割函数，主要作用时分割出图像中包含符号的最小图像。返回水平投影，垂直投影和分割图像。if_projection_img_out为1时，进行投影图像绘制
    def __img_division__(self, src, if_projection_img_out):
        imgProcess = src.copy()
        img_horizonProj = src.copy()
        img_vertialProj = src.copy()
        # 首先对图像进行水平投影
        (h1,w1) = imgProcess.shape
        a = [0 for z in range(0, h1)] #初始化一个长度为w的数组，用于记录每一行的黑点个数 
        for j in range(0,h1):  
            for i in range(0,w1):  
                if  imgProcess[j,i] == 0:
                    img_horizonProj[j,i] = 255
                    a[j]+=1
        # 再进行垂直投影
        b = [0 for i in range(0,w1)] # 初始化一个和图像列数相等的数组，用来记录每一列的黑色像素数量
        for i in range(0, w1): # 遍历列
            for j in range(0, h1): # 遍历行
                if imgProcess[j,i] == 0: # 黑点记录
                    img_vertialProj[j,i] = 255
                    b[i]+=1
        if if_projection_img_out == 1:
            for j in range(0,h1):
                for i in range(0,a[j]):
                    img_horizonProj[j,i] = 0
            for i in range(0,w1):
                for j in range(0,b[i]):
                    img_vertialProj[h1 - j - 1,i] = 0            
        # 确定符号分割位置
        horizonFlag = 0
        horizon_slice_posision = []                
        vertialFlag = 0
        vertial_slice_posision = []
        for i in range(0, h1):
            if (a[i]>0) != horizonFlag:
                horizon_slice_posision.append(i)
                horizonFlag = 1 - horizonFlag
        if len(horizon_slice_posision)%2 == 1:
            horizon_slice_posision.append(h1 - 1)  

        for i in range(0, w1):
            if (b[i]>0) != vertialFlag:
                vertial_slice_posision.append(i)
                vertialFlag = 1 - vertialFlag
        if len(vertial_slice_posision)%2 == 1:
            vertial_slice_posision.append(w1 - 1)              
        # print("切割字母的行位置",horizon_slice_posision)
        # print("切割字母的列位置",vertial_slice_posision)
        # 进行字符分割
        slice_img_part = []
        for i in range(0, int(len(vertial_slice_posision)/2)):
            if i == 0:
                slice_img_part.append(imgProcess[horizon_slice_posision[0]:horizon_slice_posision[len(horizon_slice_posision)-1],vertial_slice_posision[2*i]:vertial_slice_posision[2*i+1]])
                cv2.rectangle(imgProcess, (vertial_slice_posision[2*i], horizon_slice_posision[0]), (vertial_slice_posision[2*i+1], horizon_slice_posision[len(horizon_slice_posision)-1]), (0, 0, 255), 1)  # 在图形上画出矩形框
            else:
                slice_img_part.append(imgProcess[horizon_slice_posision[0]+4:horizon_slice_posision[len(horizon_slice_posision)-1]-3,vertial_slice_posision[2*i]:vertial_slice_posision[2*i+1]])
                cv2.rectangle(imgProcess, (vertial_slice_posision[2*i], horizon_slice_posision[0]+4), (vertial_slice_posision[2*i+1], horizon_slice_posision[len(horizon_slice_posision)-1]-3), (0, 0, 255), 1)  # 在图形上画出矩形框
            # cv2.imshow('slice_img_part_'+str(i),slice_img_part[i])
            # cv2.imshow('imgProcess',imgProcess)
        return img_horizonProj, img_vertialProj, imgProcess,slice_img_part 

    # 字母识别,对比图像src1和src2，并返回不相同的点的个数
    def __img_diff_num__(self, src1, src2):
        resultImg = np.abs(src1 - src2)
        diffNum = 0
        for i in range(0, resultImg.shape[0]):
            for j in range(0, resultImg.shape[1]):
                if resultImg[i][j] != 0:
                    diffNum+=1
        return diffNum

    # 字符比较
    def __chara_compare__(self, filePosition, img_for_compare):
        result = []  # 存储不相同的像素的个数
        imNum = 0
        while True:
            src2 = cv2.imread(filePosition + "/" + str(imNum) + ".bmp")
            if src2 is None:
                break
            src2 = cv2.cvtColor(src2, cv2.COLOR_BGR2GRAY)  # 灰度处理
            ret,src2 = cv2.threshold(src2, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)  # 二值化，0是黑色
            imNum+=1
            # print("正在比较第%d个图片"%imNum)
            a, b, c, src2_slice = self.__img_division__(src2, 0) # 对图片进行切割
            # cv2.imshow('src2_slice_'+str(imNum),src2_slice[0])
            src2_slice[0] = cv2.resize(src2_slice[0], (img_for_compare.shape[1], img_for_compare.shape[0]))
            result.append(self.__img_diff_num__(img_for_compare, src2_slice[0]))
            # cv2.imshow('modelNum_'+str(imNum),src2_slice[0])
            # cv2.imshow('img_part[0]',img_part[0])
            # print(result)
            # cv2.waitKey(0)
            # cv2.destroyAllWindows()
        print(result)
        return result.index(min(result)) # 返回最小值的位

    # 从指定位置的txt文件中取出指定行数据
    def __read_txt__(self, filePosition, whichLine):
        f = open(filePosition, "r")
        m = f.readlines()[whichLine:whichLine+1]
        f.close
        print(m)
        return m[0]


if __name__ == '__main__':
    LPR = LPR_t()
    LPR.img_process("5-carNumber/5.jpg")
    print(LPR.carNumber)
    cv2.imshow('imgRaw',LPR.imgRaw)
    cv2.imshow('imgLP',LPR.imgLP)
    cv2.imshow('imgLP_gray',LPR.imgLP_gray)
    cv2.imshow('imgLP_binary',LPR.imgLP_binary)
    cv2.imshow('imgLP_reverse_binary',LPR.imgLP_reverse_binary)
    cv2.imshow('img_division_rectangle',LPR.img_division_rectangle)
    cv2.imshow('imgLP_horizonProj',LPR.imgLP_horizonProj)
    cv2.imshow('imgLP_vertialProj',LPR.imgLP_vertialProj)
    for i in range(0, len(LPR.imgLP_slice_part)):
        cv2.imshow('imgLP_slice_part'+str(i),LPR.imgLP_slice_part[i])
    cv2.waitKey(0)
    cv2.destroyAllWindows()